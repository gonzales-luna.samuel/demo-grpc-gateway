syntax = "proto3";

package phxservices.common.types.product.v1;
option go_package = "phx-grpc-gateway/proxy_gen/phxservices/common/types/product/v1";

// Represents a role in a product breakpack relationship
enum BreakpackRole {
    BREAKPACK_ROLE_UNSPECIFIED = 0;
    BREAKPACK_ROLE_INNER = 1;
    BREAKPACK_ROLE_OUTER = 2;
}

// Identifies a product and its breakpack relationship to some other product, presumably identified by a greater message
// containing this message.
message BreakpackAssociatedProduct {
    // Required: Indicates which role the product identified below fulfills.
    phxservices.common.types.product.v1.BreakpackRole role = 1;
    // Required: A product ID.
    uint64 product_id = 2;
    // Required: The primary UPC/EAN of a product.
    string upc = 3;
}

// Describes a single price, presumably of a product at a store.
message PriceInfo {
    // Required: The list price of an H-E-B product.
    string price = 1;
    // Required: Units of product per price for the above price.
    int32 x_for = 2;
}

// Defines all the basic price info about a product at a store
message ProductPriceInfo {
    // Required: The list price of an H-E-B product.
    PriceInfo list_price = 1;
    // Required: Indicates if this product is on sale. If it is, the sale price info will be populated.
    bool on_sale = 2;
    // Optional: The list price of an H-E-B product.
    PriceInfo sale_price = 3;
}

// Represents the source of a product's delivery.
enum DeliverySource {
    DELIVERY_SOURCE_UNSPECIFIED = 0;
    DELIVERY_SOURCE_WAREHOUSE = 1;
    DELIVERY_SOURCE_VENDOR = 2;
} 

// Represents the reasons why a product cannot be shrinked.
enum ShrinkDisabledReason {
    SHRINK_DISABLED_REASON_UNSPECIFIED = 0;
    SHRINK_DISABLED_REASON_PHARMACY_DISALLOWED = 1;
    SHRINK_DISABLED_REASON_OUTER_BREAKPACK_DISALLOWED = 2;
    SHRINK_DISABLED_REASON_SUBDEPARTMENT_DISALLOWED = 3;
}
