syntax = "proto3";

package phxservices.services.store_communication.v1;

import "phxservices/common/types/store_communication/v1/store_communication.proto";

// Filter criteria to fetch store communication info.
//
// If start_at and end_at are provided, any messages with active duration overlapping
// that range are included. If only start_at is provided, any messages currently active or
// starting in the future are included.
message GetStoreCommunicationsRequest {
    // Optional: The corp ID / store number of an H-E-B store. If provided, messages for all stores are still included,
    //           but messages assigned specifically to other stores are excluded.
    int32 location_number = 1;
    // Optional: An ISO8601 timestamp. See message notes about usage. Defaults to the current moment.
    //           Expected to be UTC or have timezone specified.
    string start_at = 2;
    // Optional: An ISO8601 timestamp. See message notes about usage. Expected to be UTC or have timezone specified.
    string end_at = 3;
}

// Whatever messages exist and are relevant to the given filter criteria, if any.
message GetStoreCommunicationsResponse {
    // Required: Any applicable store communications. Might be empty.
    repeated phxservices.common.types.store_communication.v1.StoreCommunication store_communications = 1;
}

// The necessary info to create a store communication for one or more stores.
message CreateStoreCommunicationRequest {
    // Required: All the initial properties of a store communication, excluding derived details like its unique ID.
    phxservices.common.types.store_communication.v1.BaseStoreCommunicationInfo info = 1;
}

// All info about the final newly created store communication.
message CreateStoreCommunicationResponse {
    // Required: The newly created store communication.
    phxservices.common.types.store_communication.v1.StoreCommunication store_communication = 1;
}

// The necessary info to modify an existing store communication.
message EditStoreCommunicationRequest {
    // Required: The ID of an existing store communication to edit.
    uint64 store_communication_id = 1;
    // Required: All the meaningful properties of a store communication excluding things like its unique ID.
    phxservices.common.types.store_communication.v1.BaseStoreCommunicationInfo info = 2;
}

// All info about the final mutated store communication.
message EditStoreCommunicationResponse {
    // Required: The newly modified entity, including any non editable fields like created timestamp.
    phxservices.common.types.store_communication.v1.StoreCommunication store_communication = 1;
}

// The necessary info to delete an existing store communication.
message DeleteStoreCommunicationRequest {
    // Required: The ID of a store communication to delete.
    uint64 store_communication_id = 1;
}

// Nothing, because there's nothing to return.
message DeleteStoreCommunicationResponse {}

// The necessary info to get the details of a specific existing communication.
message GetStoreCommunicationByIdRequest {
    // Required: The ID of a store communication to retrieve.
    uint64 store_communication_id = 1;
}

// All info about an existing store communication.
message GetStoreCommunicationByIdResponse {
    // Required: The requested store communication's full details.
    phxservices.common.types.store_communication.v1.StoreCommunication store_communication = 1;
}

// Exposes and modifies store communications. This is exposed by phx-aviary.
service StoreCommunicationService {
    // Retrieve any applicable store communications for a given set of filters. If no filters are provided, all active
    // messages are returned.
    //
    // INVALID_ARGUMENT: Invalid store number(s). Invalid timestamp(s) format.
    rpc GetStoreCommunications(GetStoreCommunicationsRequest) returns (GetStoreCommunicationsResponse) {}

    // Create a store communication
    //
    // INVALID_ARGUMENT: Any fields were absent. Invalid store number(s). Invalid timestamp(s) format. Invalid priority.
    rpc CreateStoreCommunication(CreateStoreCommunicationRequest) returns (CreateStoreCommunicationResponse) {}

    // Modify an existing store communication.
    //
    // INVALID_ARGUMENT: Any fields were absent. Invalid store number(s). Invalid timestamp(s) format. Invalid priority.
    // NOT_FOUND: The provided store communication ID didn't exist.
    rpc EditStoreCommunication(EditStoreCommunicationRequest) returns (EditStoreCommunicationResponse) {}

    // Delete an existing store communication.
    //
    // INVALID_ARGUMENT: Store communication ID was invalid or absent.
    // NOT_FOUND: The provided store communication ID didn't exist.
    rpc DeleteStoreCommunication(DeleteStoreCommunicationRequest) returns (DeleteStoreCommunicationResponse) {}

    // Retrieve details on a specific store communication.
    //
    // INVALID_ARGUMENT: Invalid store communication ID
    // NOT_FOUND: The provided store communication ID doesn't exist.
    rpc GetStoreCommunicationById(GetStoreCommunicationByIdRequest) returns (GetStoreCommunicationByIdResponse) {}
}
