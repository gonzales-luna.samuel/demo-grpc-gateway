Run phx locally

`brew install go`

`export PATH=$PATH:$(go env GOPATH)/bin`

`make generate-grpc-gateway`

`go run cmd/app/main.go`

Before running you might need to

`cd cmd/app`
`go install`
