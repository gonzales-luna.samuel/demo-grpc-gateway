# Directory containing your .proto files
PROTO_DIR := ./proto
PROTO_DIR_THIRD_PARTY := ./ext_proto

OUTPUT_DIR_GO := ./proxy_gen
OUTPUT_DIR_OPENAPI := ./ext_gen/OpenAPI

# Default target for the make command
all: generate-grpc-gateway

generate-grpc-gateway:
	@protoc \
   		--go_out=$(OUTPUT_DIR_GO) --go_opt paths=source_relative \
   		--go-grpc_out=$(OUTPUT_DIR_GO) --go-grpc_opt paths=source_relative \
		--grpc-gateway_out=$(OUTPUT_DIR_GO) --grpc-gateway_opt paths=source_relative \
		--openapiv2_out=logtostderr=true:$(OUTPUT_DIR_OPENAPI) \
		--proto_path=$(PROTO_DIR) \
		--proto_path=$(PROTO_DIR_THIRD_PARTY) \
        ./proto/phxservices/services/ios/v1/ios_service.proto \
        ./proto/phxservices/common/types/product/v1/product.proto \
        ./proto/phxservices/common/types/partner_action/v1/partner_action.proto \
        ./proto/phxservices/common/types/psa/v1/psa.proto \
        ./proto/phxservices/common/types/tag/v1/tag.proto



